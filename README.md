Android 录屏制作GIF演示动画
==

  为了向他人展示App功能，动画效果等，往往需要将其模块功能制作成一个GIF动画。本文记录了如何制作一个GIF。

> * 手机录屏制作MP4视频文件
> * 导出MP4文件到PC
> * 使用录屏软件制作GIF

1.手机录屏制作MP4视频文件
---
使用下面命令开始MP4文件的录制。-size 指定视频分辨率，命令执行后会录屏3分钟，输入ctrl+C退出录屏。

> **adb shell screenrecord  /sdcard/yourFileName.mp4**  


2.导出MP4文件到PC
--
使用下面命令：

> **adb pull /sdcard/yourFileName.mp4  ~/mp4File/** 

3.使用录屏软件制作GIF
--
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

获取片库数据列表
==
VideoDepot
===